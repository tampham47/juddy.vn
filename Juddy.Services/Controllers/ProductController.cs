﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PbData.Business;
using PbData.Entities;

namespace Juddy.Services.Controllers
{
    public class ProductController : ApiController
    {
        [HttpGet]
        public List<pb_Product> GetAll()
        {
            bn_Product bnProduct = new bn_Product(isLazy: false);
            return bnProduct.GetAll();
        }

        [HttpGet]
        public pb_Product GetById(Guid productId)
        {
            //
            bn_Product bnProduct = new bn_Product(isLazy: false);
            return bnProduct.GetById(productId);
        }

        [HttpGet]
        public List<pb_Product> Filter(string tag)
        {
            bn_Product bnProduct = new bn_Product(isLazy: false);
            return bnProduct.GetByTagName(tag);
        }

        public bool Favourite(Guid productId, Guid userId)
        {
            return false;
        }

        //return list of product depend on page index and products per page
        [HttpGet]
        public List<pb_Product> GetByPage(int pageIndex, int pageSize )
        {                  
            bn_Product bnProduct = new bn_Product(isLazy: false);
            return bnProduct.GetByPage(pageIndex, pageSize);            
        }
    }
}
